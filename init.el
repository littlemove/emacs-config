;;; init.el

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;;----------------------------------------------------------------------------
;; Allow access from emacsclient
;;----------------------------------------------------------------------------
(require 'server)
(unless (server-running-p)
  (server-start))

;; Load path etc.
(setq dotfiles-dir (file-name-directory "~/.emacs.d/elisp/"))
(add-to-list 'load-path dotfiles-dir)

;;----------------------------------------------------------------------------
;; Which functionality to enable (use t or nil for true and false)
;;----------------------------------------------------------------------------
(setq *spell-check-support-enabled* nil)
(setq *macbook-pro-support-enabled* t)
(setq *is-a-mac* (eq system-type 'darwin))
(setq *is-carbon-emacs* (and *is-a-mac* (eq window-system 'mac)))
(setq *is-cocoa-emacs* (and *is-a-mac* (eq window-system 'ns)))

(require 'osx-keys)

(require 'exec-path)
(require 'keybindings)
(require 'defuns)
(require 'misc)

(require 'registers)

(require 'cl)
(require 'saveplace)
(require 'ffap)
(require 'uniquify)
(require 'ansi-color)
(require 'recentf)

;; el-get setup
(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil t)
  (url-retrieve
   "https://github.com/dimitri/el-get/raw/master/el-get-install.el"
   (lambda (s)
     (let (el-get-master-branch)
       (end-of-buffer)
       (eval-print-last-sexp)))))

(setq el-get-byte-compile nil)
(setq el-get-user-package-directory "~/.emacs.d/elisp/inits")

(setq el-get-sources '(inf-ruby
                       ruby-electric
                       flymake-ruby
                       rbenv
                       helm
                       helm-ag
                       helm-fuzzier
                       color-theme
                       color-theme-railscasts
                       color-theme-solarized
                       yaml-mode
                       paredit
                       yasnippet
                       markdown-mode
                       package
                       csv-mode
                       csv-nav
                       bundler
                       dash-at-point
                       web-mode
                       projectile
                       projectile-rails
                       ag
                       magit
                       powerline
                       helm-gtags
                       lua-mode
                       json-mode
                       editorconfig
                       company-mode))

(el-get 'sync el-get-sources)

;; Theme, fonts, ...
(set-face-attribute 'default nil :family "Source Code Pro" :height 140)
(color-theme-solarized-dark)

;; To move
;; Flyspell related config
(setq ispell-program-name "aspell"
      ispell-list-command "list"
      ispell-local-dictionary "en")

(dolist (hook '(text-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))
(dolist (hook '(change-log-mode-hook log-edit-mode-hook html-mode-hook))
  (add-hook hook (lambda () (flyspell-mode -1))))

(global-set-key (kbd "<C-M-tab>") 'flyspell-auto-correct-word)


(add-hook 'before-save-hook (lambda () (delete-trailing-whitespace)))

;; Don't truncate
(setq truncate-lines t
      truncate-partial-width-windows nil)

;; Line numbers for every buffer
(global-linum-mode)
;; to move end

(require 'ruby)
(require 'lisp)

(require 'uniquify)
(setq uniquify-buffer-name-style 'reverse)
(setq uniquify-separator " • ")
(setq uniquify-after-kill-buffer-p t)
(setq uniquify-ignore-buffers-re "^\\*")

;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-save-interval 10)
 '(auto-save-timeout 3)
 '(json-reformat:indent-width 2))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(helm-selection ((t (:background "#073642" :distant-foreground "black")))))
