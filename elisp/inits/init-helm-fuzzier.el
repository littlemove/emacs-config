(defun my-helm-make-source (f &rest args)
  (nconc args '(:fuzzy-match t))
  (apply f args))
(advice-add 'helm-make-source :around 'my-helm-make-source)
